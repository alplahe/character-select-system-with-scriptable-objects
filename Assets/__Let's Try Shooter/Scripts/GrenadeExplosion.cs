﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GrenadeExplosion : MonoBehaviour
{
	public float m_radius = 5.0F;
	public float m_power = 500.0F;
	public float m_upwardsModifier = 0.0F;
	public float m_timeDelay = 2.0F;
	public GameObject m_explosionParticlesPrefab;

	void Start()
	{
		StartCoroutine(WaitAndExplode());
	}

	private IEnumerator WaitAndExplode()
	{
		yield return new WaitForSeconds(m_timeDelay);
		Explode();
	}

	private void Explode()
	{
		Vector3 explosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPos, m_radius);
		foreach (Collider hit in colliders)
		{
			Rigidbody rigidBody = hit.GetComponent<Rigidbody>();

			if (rigidBody != null)
			{
				rigidBody.AddExplosionForce(m_power, explosionPos, m_radius, m_upwardsModifier);
			}
		}
		
		if (m_explosionParticlesPrefab)
		{
			GameObject explosion = (GameObject)Instantiate (
				m_explosionParticlesPrefab, transform.position, m_explosionParticlesPrefab.transform.rotation);
			Destroy (explosion, explosion.GetComponent<ParticleSystem> ().main.startLifetimeMultiplier);
		}
		
		Destroy(gameObject);
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, m_radius);
	}
}
