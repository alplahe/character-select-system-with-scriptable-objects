﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelector : MonoBehaviour
{
	public GameObject m_player;
	public Vector3 m_playerSpawnPosition = new Vector3(0, 1, -7);
	public Character[] m_characters;

	public GameObject m_characterSelectPanel;
	public GameObject m_abilityPanel;
	
	public void OnCharacterSelect(int characterChoice)
	{
		m_characterSelectPanel.SetActive(false);
		m_abilityPanel.SetActive(true);
		GameObject spawnedPlayer = Instantiate(m_player, m_playerSpawnPosition, Quaternion.identity) as GameObject;
		WeaponMarker weaponMarker = spawnedPlayer.GetComponentInChildren<WeaponMarker>();
		AbilityCoolDown[] coolDownButtons = GetComponentsInChildren<AbilityCoolDown>();
		Character selectedCharacter = m_characters[characterChoice];

		for (int i = 0; i < coolDownButtons.Length; i++)
		{
			coolDownButtons[i].Initialize(selectedCharacter.m_characterAbilities[i], weaponMarker.gameObject);
		}
	}
}
