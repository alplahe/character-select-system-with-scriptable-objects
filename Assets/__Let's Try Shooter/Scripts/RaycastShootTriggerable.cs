﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShootTriggerable : MonoBehaviour
{
	[HideInInspector] public int m_gunDamage = 1;
//	[HideInInspector] public float m_fireRate = 0.25f;
	[HideInInspector] public float m_weaponRange = 50f;
	[HideInInspector] public float m_hitForce = 100f;
	public Transform m_gunEnd;
	[HideInInspector] public LineRenderer m_laserLine;

	private Camera m_fpsCamera;
	private WaitForSeconds m_shotDuration = new WaitForSeconds(0.07f);
//	private float m_nextFire;

	// Use this for initialization
	public void Initialize ()
	{
		m_laserLine = GetComponent<LineRenderer>();
		m_fpsCamera = GetComponentInParent<Camera>();
	}

	public void Fire()
	{
		
//		UpdateNextFire();

		StartCoroutine(ShotEffect());

		SetLaserLine();
	}

	private void UpdateNextFire()
	{
//		m_nextFire = Time.time + m_fireRate;
	}

	private IEnumerator ShotEffect()
	{
		m_laserLine.enabled = true;
		yield return m_shotDuration;
		m_laserLine.enabled = false;
	}

	private void SetLaserLine()
	{
		Vector3 rayOrigin = SetRayOriginToTheCenterOfTheScreen();
		RaycastHit raycastHit;

		SetLaserOriginPosition();

		if (IsRaycastHit(rayOrigin, out raycastHit))
		{
			SetLaserEndPositionToHitPoint(raycastHit);

			DoDamageAndPhysicResponse(raycastHit);
		}
		else
		{
			SetLaserEndPositionToWeaponRange(rayOrigin);
		}
	}

	private Vector3 SetRayOriginToTheCenterOfTheScreen()
	{
		return m_fpsCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
	}

	private void SetLaserOriginPosition()
	{
		m_laserLine.SetPosition(0, m_gunEnd.position);
	}

	private bool IsRaycastHit(Vector3 rayOrigin, out RaycastHit raycastHit)
	{
		return Physics.Raycast(rayOrigin, m_fpsCamera.transform.forward, out raycastHit, m_weaponRange);
	}

	private void SetLaserEndPositionToHitPoint(RaycastHit raycastHit)
	{
		m_laserLine.SetPosition(1, raycastHit.point);
	}

	private void DoDamageAndPhysicResponse(RaycastHit raycastHit)
	{
		ShootableBox gameobjectHit = raycastHit.collider.GetComponent<ShootableBox>();
		DoDamage(gameobjectHit);
		DoPhysicResponse(raycastHit);
	}

	private void DoDamage(ShootableBox health)
	{
		if (health != null)
		{
			health.Damage(m_gunDamage);
		}
	}

	private void DoPhysicResponse(RaycastHit raycastHit)
	{
		if (raycastHit.rigidbody != null)
		{
			raycastHit.rigidbody.AddForce(-raycastHit.normal * m_hitForce);
		}
	}

	private void SetLaserEndPositionToWeaponRange(Vector3 rayOrigin)
	{
		m_laserLine.SetPosition(1, rayOrigin + (m_fpsCamera.transform.forward * m_weaponRange));
	}
}
