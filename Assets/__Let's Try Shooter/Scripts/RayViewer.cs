﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayViewer : MonoBehaviour
{
	public float m_weaponRange = 50f;

	private Camera m_fpsCamera;

	// Use this for initialization
	void Start ()
	{
		m_fpsCamera = GetComponentInParent<Camera>();
	}

	// Update is called once per frame
	void Update ()
	{
		Vector3 lineOrigin = SetLineOriginToTheCenterOfTheScreen();
		Debug.DrawRay(lineOrigin, m_fpsCamera.transform.forward * m_weaponRange, Color.green);
	}

	private Vector3 SetLineOriginToTheCenterOfTheScreen()
	{
		return m_fpsCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
	}
}
