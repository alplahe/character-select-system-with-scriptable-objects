﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosionFadeOut : MonoBehaviour
{
	public AudioSource m_audioSource;

	// Use this for initialization
	void Start ()
	{
		StartCoroutine(FadeOut(m_audioSource, 0.82f));
	}
	
	private static IEnumerator FadeOut (AudioSource audioSource, float FadeTime)
	{
		float startVolume = audioSource.volume;
 
		while (audioSource.volume > 0)
		{
			audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
 
			yield return null;
		}
 
		audioSource.Stop ();
		audioSource.volume = startVolume;
	}
}
