﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Character")]
public class Character : ScriptableObject
{
	public string m_characterName = "Default";
	public int m_startingHP = 100;

	public Ability[] m_characterAbilities;
}
